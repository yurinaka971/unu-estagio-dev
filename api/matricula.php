<?php

require 'config.php';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = json_decode(file_get_contents('php://input'), true);
    $request_method=$_SERVER["REQUEST_METHOD"];
    switch($request_method)
    {
        case 'GET':
            echo get_matricula($_GET["id"]);
            break;
        case 'POST':
            echo new_matricula($data);
            break;
        case 'PUT':
            echo update_matricula($data);
            break;
        case 'DELETE':
            echo delete_matricula($data);
            break;
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
}
catch(PDOException $e){
    echo json_encode($sql . " - " . $e->getMessage());
}

$conn = null;

function get_matricula($user_id=1) {
    global $conn;

    $sql = "SELECT matriculas.id, alunos.nome, disciplinas.id AS disciplina_id, disciplinas.nome AS disciplina, matriculas.periodo
    FROM (alunos INNER JOIN matriculas ON alunos.id = matriculas.aluno_id)
    INNER JOIN disciplinas ON disciplinas.id = matriculas.disciplina_id
    WHERE alunos.id = ".$user_id." AND matriculas.deleted_at IS NULL";

    $stmt = $conn->prepare($sql); 
    $stmt->execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

    return json_encode($stmt->fetchAll());
}

function new_matricula($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $stmt = $conn->prepare("INSERT INTO matriculas (periodo, aluno_id, disciplina_id) 
        VALUES (:periodo, :aluno_id, :disciplina_id)");
        $stmt->bindParam(':periodo', $periodo);
        $stmt->bindParam(':aluno_id', $aluno_id);
        $stmt->bindParam(':disciplina_id', $disciplina_id);

        $periodo = intval($data["periodo"]);
        $aluno_id = intval($data["aluno_id"]);
        $disciplina_id = intval($data["disciplina_id"]);
        $stmt->execute();

        return json_encode("{'log': 'success'}");
    }
}

function update_matricula($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $sql = "UPDATE matriculas
        SET periodo=".$data["periodo"]
            .", aluno_id=".$data["aluno_id"]
            .", disciplina_id=".$data["disciplina_id"]
            .", updated_at=NOW()"
        ."WHERE id=".$data["id"]." AND deleted_at IS NULL";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount() . " matriculas UPDATED successfully";
    }
}

function delete_matricula($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $sql = "UPDATE matriculas SET deleted_at=NOW() WHERE aluno_id=".$data["aluno_id"]." AND deleted_at IS NULL";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount() . " matricula DELETED successfully";
    }
}

?>