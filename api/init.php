<?php

require 'config.php';

try {
    $conn = new PDO("mysql:host=$servername", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result[] = "Connected successfully";

    $sql = "CREATE DATABASE cadastro";
    $conn->exec($sql);
    $result[] = "Database created successfully";

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    $sql = "CREATE TABLE alunos (
        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
        nome VARCHAR(255) NOT NULL,
        data_nascimento DATE,
        created_at DATETIME DEFAULT NOW(),
        updated_at DATETIME,
        deleted_at DATETIME
    )";
    $conn->exec($sql);
    $result[] = "Table alunos created successfully";

    $sql = "CREATE TABLE disciplinas (
        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        nome VARCHAR(255) NOT NULL UNIQUE,
        created_at DATETIME DEFAULT NOW(),
        updated_at DATETIME,
        deleted_at DATETIME
    )";
    $conn->exec($sql);
    $result[] = "Table disciplinas created successfully";

    $sql = "CREATE TABLE matriculas (
        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        periodo INT NOT NULL,
        aluno_id INT UNSIGNED NOT NULL,
        disciplina_id INT UNSIGNED NOT NULL,
        created_at DATETIME DEFAULT NOW(),
        updated_at DATETIME,
        deleted_at DATETIME
    )";
    $conn->exec($sql);
    $result[] = "Table matriculas created successfully";

    $sql = "CREATE TABLE notas (
        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        aluno_id INT UNSIGNED NOT NULL,
        disciplina_id INT UNSIGNED NOT NULL,
        nota FLOAT(3,2) NOT NULL,
        created_at DATETIME DEFAULT NOW(),
        updated_at DATETIME,
        deleted_at DATETIME
    )";
    $conn->exec($sql);
    $result[] = "Table notas created successfully";

    echo json_encode($result);
}
catch(PDOException $e){
    $result[] = $sql . " - " . $e->getMessage();
    echo json_encode($result);
}
    $conn = null;

?>