<?php

require 'config.php';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = json_decode(file_get_contents('php://input'), true);
    $request_method=$_SERVER["REQUEST_METHOD"];
    switch($request_method)
    {
        case 'GET':
            echo get_disciplina();
            break;
        case 'POST':
            echo new_disciplina($data);
            break;
        case 'PUT':
            echo update_disciplina($data);
            break;
        case 'DELETE':
            echo delete_disciplina($data);
            break;
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
}
catch(PDOException $e){
    echo json_encode($sql . " - " . $e->getMessage());
}

$conn = null;

function get_disciplina() {
    global $conn;

    $sql = 'SELECT id, nome FROM disciplinas WHERE deleted_at IS NULL';

    $stmt = $conn->prepare($sql); 
    $stmt->execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

    return json_encode($stmt->fetchAll());
}

function new_disciplina($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $stmt = $conn->prepare("INSERT INTO disciplinas (nome) 
        VALUES (:nome)");
        $stmt->bindParam(':nome', $nome);

        $nome = $data["nome"];
        $stmt->execute();

        return json_encode("{'log': 'success'}");
    }
}

function update_disciplina($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $sql = "UPDATE disciplinas
        SET nome='".$data["nome"]
        ."', updated_at=NOW()
        WHERE id=".$data["id"]." AND deleted_at IS NULL";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount() . " disciplinas UPDATED successfully";
    }
}

function delete_disciplina($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $sql = "UPDATE disciplinas
        SET deleted_at=NOW()
        WHERE id=".$data["id"]." AND deleted_at IS NULL";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount() . " disciplina DELETED successfully";
    }
}

?>