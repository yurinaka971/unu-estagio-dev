<?php

require 'config.php';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = json_decode(file_get_contents('php://input'), true);
    $request_method=$_SERVER["REQUEST_METHOD"];
    switch($request_method)
    {
        case 'GET':
            echo get_aluno();
            break;
        case 'POST':
            echo new_aluno($data);
            break;
        case 'PUT':
            echo update_aluno($data);
            break;
        case 'DELETE':
            echo delete_aluno($data);
            break;
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
}
catch(PDOException $e){
    echo json_encode($sql . " - " . $e->getMessage());
}

$conn = null;

function get_aluno() {
    global $conn;

    $sql = 'SELECT id, nome, data_nascimento FROM alunos WHERE deleted_at IS NULL';

    $stmt = $conn->prepare($sql); 
    $stmt->execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

    return json_encode($stmt->fetchAll());
}

function new_aluno($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $stmt = $conn->prepare("INSERT INTO alunos (nome, data_nascimento) 
        VALUES (:nome, :data_nascimento)");
        $stmt->bindParam(':nome', $nome);
        $stmt->bindParam(':data_nascimento', $data_nascimento);

        $nome = $data["nome"];
        $data_nascimento = $data["data_nascimento"];
        $stmt->execute();

        return json_encode("{'log': 'success'}");
    }
}

function update_aluno($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $sql = "UPDATE alunos
        SET nome='".$data["nome"]
        ."', data_nascimento='".$data["data_nascimento"]
        ."', updated_at=NOW()"
        ."WHERE id=".$data["id"]." AND deleted_at IS NULL";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount() . " alunos UPDATED successfully";
    }
}

function delete_aluno($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $sql = "UPDATE alunos
        SET deleted_at=NOW()
        WHERE id=".$data["id"]." AND deleted_at IS NULL";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount() . " aluno DELETED successfully";
    }
}

?>
