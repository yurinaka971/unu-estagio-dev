<?php

require 'config.php';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = json_decode(file_get_contents('php://input'), true);
    $request_method=$_SERVER["REQUEST_METHOD"];
    switch($request_method)
    {
        case 'GET':
            echo get_nota($_GET["id"]);
            break;
        case 'POST':
            echo new_nota($data);
            break;
        case 'PUT':
            echo update_nota($data);
            break;
        case 'DELETE':
            echo delete_nota($data);
            break;
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
}
catch(PDOException $e){
    echo json_encode($sql . " - " . $e->getMessage());
}

$conn = null;

function get_nota($user_id) {
    global $conn;

    if(!$user_id) {
        return json_encode('Invalid data');
    }

    $sql = "SELECT notas.id, disciplinas.nome AS disciplina, notas.nota
    FROM (notas INNER JOIN alunos ON alunos.id = notas.aluno_id)
    INNER JOIN disciplinas ON disciplinas.id = notas.disciplina_id
    WHERE alunos.id = ".$user_id." AND notas.deleted_at IS NULL";

    $stmt = $conn->prepare($sql); 
    $stmt->execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

    return json_encode($stmt->fetchAll());
}

function new_nota($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $stmt = $conn->prepare("INSERT INTO notas (aluno_id, disciplina_id, nota) 
        VALUES (:aluno_id, :disciplina_id, :nota)");
        $stmt->bindParam(':aluno_id', $aluno_id);
        $stmt->bindParam(':disciplina_id', $disciplina_id);
        $stmt->bindParam(':nota', $nota);

        $aluno_id = intval($data["aluno_id"]);
        $disciplina_id = intval($data["disciplina_id"]);
        $nota = $data["nota"];
        $stmt->execute();

        return json_encode("{'log': 'success'}");
    }
}

function update_nota($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $sql = "UPDATE notas
        SET nota=".$data["nota"]
            .", updated_at=NOW() 
        WHERE id=".$data["id"]." AND deleted_at IS NULL";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount() . " notas UPDATED successfully";
    }
}

function delete_nota($data) {
    global $conn;

    if(!$data) {
        return json_encode('Invalid data');
    }
    else {
        $sql = "UPDATE notas
        SET deleted_at=NOW()
        WHERE id=".$data["id"]." AND deleted_at IS NULL";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount() . " matricula DELETED successfully";
    }
}

?>