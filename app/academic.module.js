(function() {
	
    'use strict';

    angular.module('academic', [
    	'ui.router',
    	'academic.common',
    	'academic.aluno',
    	'academic.disciplina',
    	'academic.matricula'
    ]);
})();
