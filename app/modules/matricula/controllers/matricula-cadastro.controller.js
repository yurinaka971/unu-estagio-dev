(function () {

	'use strict';

	angular.module('academic.matricula')
		.controller('MatriculaCadastroController', MatriculaCadastroController);

	function MatriculaCadastroController (AlunoDataBaseService, DisciplinaDataBaseService, MatriculaDataBaseService) {
		var vm = this;

		vm.submit = submit;
		vm.del = false;

		_init();


		function _init() {
			AlunoDataBaseService.get()
				.then(function(data) {
					vm.alunos = data;
				});
			DisciplinaDataBaseService.get()
				.then(function(data) {
					vm.disciplinas = data;
				})
		}

		function submit() {
			console.log(vm.periodo, vm.aluno, vm.disciplina);
			MatriculaDataBaseService.post(vm.periodo, vm.aluno, vm.disciplina)
				.then(function(data) {
					console.log(data);
				})
		}
	}

})();