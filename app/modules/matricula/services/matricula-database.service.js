(function () {

	'use strict';

	angular.module('academic.matricula')
		.service('MatriculaDataBaseService', MatriculaDataBaseService);

	function MatriculaDataBaseService ($q, $http) {
		var service = this;

		service.matricula       = {};
		service.get             = get;
		service.post            = post;
		service.put             = put;

		function get (aluno) {
			var deferred = $q.defer();

			$http.get('http://localhost/estagio/api/matricula.php?id=' + aluno.id)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}

		function post (periodo, aluno, disciplina) {
			var deferred = $q.defer();
			var matricula = {
				periodo,
				aluno_id: aluno.id,
				disciplina_id: disciplina.id
			};

			$http.post('http://localhost/estagio/api/matricula.php', matricula)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}

		function put (matricula, del) {
			service.matricula = matricula;
			var deferred = $q.defer();

			console.log(matricula);
			if(del)
				$http.delete('http://localhost/estagio/api/matricula.php', {data: matricula})
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});
			else
				$http.put('http://localhost/estagio/api/matricula.php', matricula)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}
	}

})();