(function () {

	'use strict';

	angular.module('academic.aluno')
		.service('AlunoDataBaseService', AlunoDataBaseService);

	function AlunoDataBaseService ($q, $http) {
		var service = this;

		service.aluno           = {};
		service.get             = get;
		service.post            = post;
		service.put             = put;
		service.nascimento      = nascimento;

		function get () {
			var deferred = $q.defer();

			$http.get('http://localhost/estagio/api/aluno.php')
				.then(function(response) {
					deferred.resolve(viewNascimento(response.data));
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}

		function post (aluno) {
			service.aluno = aluno;
			nascimento();
			var deferred = $q.defer();

			$http.post('http://localhost/estagio/api/aluno.php', aluno)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}

		function put (aluno, del) {
			service.aluno = aluno;
			nascimento();
			var deferred = $q.defer();

			console.log(aluno);
			if(del)
				$http.delete('http://localhost/estagio/api/aluno.php', {data: aluno})
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});
			else
				$http.put('http://localhost/estagio/api/aluno.php', aluno)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}

		function nascimento() {
			var data = '';

			data += service.aluno.ano
				 + '-'
				 + service.aluno.mes
				 + '-'
				 + service.aluno.dia;

			service.aluno.data_nascimento = data;
		}

		function viewNascimento(alunos) {
			for(var aluno of alunos) {
				var data = aluno.data_nascimento.split('-');
				aluno.ano = parseInt(data[0]);
				aluno.mes = parseInt(data[1]);
				aluno.dia = parseInt(data[2]);
			}

			return alunos;
		}
	}

})();