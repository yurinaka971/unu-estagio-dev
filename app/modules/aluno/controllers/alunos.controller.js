(function () {

	'use strict';

	angular.module('academic.aluno')
		.controller('AlunosController', AlunosController);


	// AlunosController.$inject['$http'];
	function AlunosController($state, $http, AlunoDataBaseService) {
		var vm = this;

		vm.show = show;
		_init();


		function _init() {
			AlunoDataBaseService.get()
				.then(function(data) {
					vm.alunos = data;
				});
		}

		function show (aluno, edit) {
			AlunoDataBaseService.aluno = aluno;
			console.log(aluno);
			if(edit)
				$state.go('aluno-modifica');
			else
				$state.go('aluno-detalhe');
		}
	}
})();