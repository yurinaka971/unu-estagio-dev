(function () {

	'use strict';

	angular.module('academic.aluno')
		.controller('AlunoModificaController', AlunoModificaController);

	function AlunoModificaController (AlunoDataBaseService) {
		var vm = this;

		vm.submit = submit;
		vm.apagar = apagar;
		vm.del = true;

		_init();

		function _init() {
			vm.aluno = AlunoDataBaseService.aluno;
		}

		function submit() {
			console.log(vm.aluno);
			AlunoDataBaseService.put(vm.aluno, false)
				.then(function(data) {
					console.log('Updated');
				})
		}

		function apagar() {
			console.log(vm.aluno);
			AlunoDataBaseService.put(vm.aluno, true)
				.then(function(data) {
					console.log(data);
				})
		}
	}

})();