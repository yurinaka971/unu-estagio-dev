(function () {

	'use strict';

	angular.module('academic.aluno')
		.controller('AlunoDetalheController', AlunoDetalheController);

	function AlunoDetalheController (AlunoDataBaseService, MatriculaDataBaseService) {
		var vm = this;

		vm.aluno = AlunoDataBaseService.aluno;
		vm.media = 7.50;

		_init();


		function _init() {
			MatriculaDataBaseService.get(AlunoDataBaseService.aluno)
				.then(function(data) {
					vm.detalhe = data;
					console.log(data);
				})
		}

		// function viewData (response) {
		// 	var result = [];
		// 	var periodo = 0;

		// 	for(var data in response) {
		// 		if(!result[response.periodo])
		// 			result[response.periodo] = {response.periodo:[]};
		// 		result[response.periodo].push(response);
		// 	}

		// 	return result;
		// }
	}

})();
