(function () {

	'use strict';

	angular.module('academic.aluno')
		.controller('AlunoCadastroController', AlunoCadastroController);

	function AlunoCadastroController (AlunoDataBaseService) {
		var vm = this;

		vm.submit = submit;
		vm.del = false;

		function submit() {
			console.log(vm.aluno);
			
			AlunoDataBaseService.post(vm.aluno)
				.then(function(data) {
					console.log(data);
				}, function(err) {
					console.log(err);
				})
		}
	}

})();