(function () {

	'use strict';

	angular.module('academic.common')
		.service('DataBaseService', DataBaseService);

	function DataBaseService () {
		var service = this;

		service.alunos = [];
		service.disciplinas = [];

		service.getAlunos = getAlunos;
		service.postAluno = postAluno;
		service.putAluno = putAluno;

		service.getDisciplinas = getDisciplinas;
		service.postDisciplina = postDisciplina;
		service.putDisciplina = putDisciplina;
	}

})();