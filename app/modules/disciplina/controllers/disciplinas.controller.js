(function () {

	'use strict';

	angular.module('academic.disciplina')
		.controller('DisciplinasController', DisciplinasController);

	function DisciplinasController ($state, DisciplinaDataBaseService) {
		var vm = this;

		vm.show = show;

		vm.disciplinas = [
			{id: 1, nome: "Cálculo 1"},
			{id: 1, nome: "Cálculo 1"},
			{id: 1, nome: "Cálculo 1"},
			{id: 1, nome: "Cálculo 1"},
			{id: 1, nome: "Cálculo 1"},
		];

		_init();

		function _init() {
			DisciplinaDataBaseService.get()
				.then(function(data) {
					vm.disciplinas = data;
				});
		}

		function show(disciplina) {
			DisciplinaDataBaseService.disciplina = disciplina;
			console.log(disciplina);
			$state.go('disciplina-detalhe');
		}
	}

})();