(function () {

	'use strict';

	angular.module('academic.disciplina')
		.controller('DisciplinaCadastroController', DisciplinaCadastroController);

	function DisciplinaCadastroController (DisciplinaDataBaseService) {
		var vm = this;

		vm.submit = submit;
		vm.del = false;

		function submit() {
			console.log(vm.disciplina);
			DisciplinaDataBaseService.post(vm.disciplina)
				.then(function(data) {
					console.log(data);
				})
		}
	}

})();