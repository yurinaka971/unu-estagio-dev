(function () {

	'use strict';

	angular.module('academic.disciplina')
		.controller('DisciplinaDetalheController', DisciplinaDetalheController);

	function DisciplinaDetalheController (DisciplinaDataBaseService) {
		var vm = this;

		vm.del = true;
		vm.submit = submit;
		vm.apagar = apagar;
		
		_init();

		function _init () {
			vm.disciplina = DisciplinaDataBaseService.disciplina;
		}

		function submit () {
			console.log(vm.disciplina);
			DisciplinaDataBaseService.put(vm.disciplina, false)
				.then(function(data) {
					console.log(data);
				})
		}

		function apagar() {
			console.log(vm.disciplina);
			DisciplinaDataBaseService.put(vm.disciplina, true)
				.then(function(data) {
					console.log(data);
				})
		}
	}

})();
