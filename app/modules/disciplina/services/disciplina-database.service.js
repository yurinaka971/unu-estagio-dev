(function () {

	'use strict';

	angular.module('academic.disciplina')
		.service('DisciplinaDataBaseService', DisciplinaDataBaseService);

	function DisciplinaDataBaseService ($q, $http) {
		var service = this;

		service.disciplina      = {};
		service.get             = get;
		service.post            = post;
		service.put             = put;

		function get () {
			var deferred = $q.defer();

			$http.get('http://localhost/estagio/api/disciplina.php')
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}

		function post (disciplina) {
			var deferred = $q.defer();

			$http.post('http://localhost/estagio/api/disciplina.php', disciplina)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}

		function put (disciplina, del) {
			service.disciplina = disciplina;
			var deferred = $q.defer();

			console.log(disciplina);
			if(del)
				$http.delete('http://localhost/estagio/api/disciplina.php', {data: disciplina})
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});
			else
				$http.put('http://localhost/estagio/api/disciplina.php', disciplina)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(err) {
					deferred.reject(err);
				});

			return deferred.promise;
		}
	}

})();